# Autocare

Version: 1.0

## Author:

SuperFastBusiness

## Summary

Autocare is a Genesis child theme made for Auto servicing industries in general. 

### Deployment

* Clone repository
* Install dependencies
    * [Ruby](http://rubyinstaller.org/)
    * [Node.js](https://nodejs.org/)
    * [Compass](http://compass-style.org)
* Open Command Line and run `npm install` to install gulp and dependencies.
* Run `bower install` to install packages.
* Run `gulp` to compile scripts and stylesheet.

### Installation

* Clone/download files from Bitbucket repository.
* Copy/extract/upload to WordPress themes folder.
* If you don't want to use Gulp replace `style.css` with `temp/css/style.css` and `assets\js\app.min.js` with `temp/js/app.js` to use the uncompressed files.

### Features

* Compass, SCSS and easy-to-use of mixins ready to go
* Easy to customize
* Gulp
* Child theme tweaks
    * WordPress `head` cleanup
    * Before Footer Hook `mb_before_footer`
    * Before Footer Widget Area Hook `mb_before_footer_widget_area`
    * Additional widget areas
    * Many more...
* Built-in Vafpress Theme Options Framework support
* Built-in TGM Plugin Activation support
* [WP-LESS](https://github.com/sanchothefat/wp-less) support
* Theme Updater using [GitHub Updater](https://github.com/afragen/github-updater)
* [AniJS](http://anijs.github.io/) support
* [Font Awesome](https://fortawesome.github.io/) support
* Built-in [FitVids](http://fitvidsjs.com/) support for responsive videos.
* CSS animations using [Animate.css](https://daneden.github.io/animate.css/)

### Suggested Plugins

The plugins below are already added into the [TGM Plugin Activation](http://tgmpluginactivation.com/) script. Just install them after activating the theme.

* [Page Builder by SiteOrigin](https://wordpress.org/plugins/siteorigin-panels/)
* [SiteOrigin Widgets Bundle](https://wordpress.org/plugins/so-widgets-bundle/)
* [GitHub Updater](https://github.com/afragen/github-updater)
* [Developer Bundle](https://bitbucket.org/webdevsuperfast/developer-bundle)

### Credits

Without these projects, this WordPress Genesis Starter Child Theme wouldn't be where it is today.

* [Genesis Framework](http://my.studiopress.com/themes/genesis/)
* [SASS / SCSS](http://sass-lang.com/)
* [Less](http://lesscss.org/)
* [Compass](http://compass-style.org)
* [Gulp](http://gulpjs.com/)
* [Bower](https://github.com/bower/bower)
* [WP-Less](https://github.com/sanchothefat/wp-less)
* [TGM Plugin Activation](http://tgmpluginactivation.com/)
* [Font Awesome](https://fortawesome.github.io/Font-Awesome/)
* [Animate.css](https://daneden.github.io/animate.css/)
* [FitVids](http://fitvidsjs.com/)
* [AniJS](http://anijs.github.io/)
* [CSS Tricks](https://css-tricks.com)

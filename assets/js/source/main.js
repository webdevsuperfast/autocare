(function($) {
	"use strict";

	$(document).ready(function(){
		// Add class to images with anchor links
		$('.content').find('img').parent('a').addClass('imgwrap');

		// Smooth Go to Top
		$('body').append('<div class="gototop"><span class="fa fa-chevron-up"></span></div>');

		var gototop = $('.gototop, .gotobottom');
		$(window).scroll(function(){
			if ($(this).scrollTop() > 200 ){
				gototop.fadeIn(500);
				gototop.addClass('top');
			} else {
				gototop.fadeOut(500);
				gototop.removeClass('top');
			}
		});

		$('.gototop').click(function () {
			$('html, body').animate({
				scrollTop: '0px'
			}, 500);
			return false;
		});

		// Responsive Menu
		$( '.genesis-nav-menu' ).before( '<button class="menu-toggle" role="button" aria-pressed="false"></button>' ); // Add toggles to menus
		$( '.genesis-nav-menu .sub-menu' ).before( '<button class="sub-menu-toggle" role="button" aria-pressed="false"></button>' ); // Add toggles to sub menus

		// Show/hide the navigation
		$( '.menu-toggle, .sub-menu-toggle' ).on( 'click', function() {
			var $this = $( this );
			$this.attr( 'aria-pressed', function( index, value ) {
				return 'false' === value ? 'true' : 'false';
			});

			$this.toggleClass( 'activated' );
			$this.next( '.genesis-nav-menu, .sub-menu' ).slideToggle( 'fast' );

		});

		// FitVids
		var container = $( '.video, .content' );
		container.fitVids();
		container.fitVids( {
			customSelector: "iframe[src*='//fast.wistia.net']"
		} );
		$('.content p').each(function() {
			var $this = $(this);
			if($this.html().replace(/\s| /g, '').length === 0){
				$this.addClass('empty');
			}
		});

		var windowResize = function() {
			// Home Top 1
			if ($('.home-top-1').is('.margin')) {
				var $$ = $('.home-top-1');
				if (document.documentElement.clientWidth >= 780) {
					$$.css('margin-top', -($$.height()/2));
					$$.find('.widget').matchHeight();
				}
			}
		};

		$(window).resize(windowResize);
		windowResize();
	});

	(function fn() {
		fn.now = +new Date;
		$(window).load(function() {
			if (+new Date - fn.now < 100) {
				setTimeout(fn, 100);
			}
			var foo = $('body');
			foo.addClass('animated fadeIn');
			foo.stop().fadeIn(300);

			function updateSize(){
        	    var minHeight=parseInt(homeBottom.find('.widget_mb_image').eq(0).css('height'));
        	    $('.imagewidget').each(function () {
        	        var $$ = $(this), thisHeight = $$.height();
        	        $$.css('min-height', thisHeight);
        	        if (minHeight >= thisHeight) {
        	            $$.css('margin-top', (minHeight-thisHeight)/2);
        	        }
        	    });
        	}

            foo.css('visibility', 'visible');

            if ($('body').is('.home')) {
            	var homeBottom = $('.home-bottom-2');
            	updateSize();
            }
		});
	})();
})(jQuery);
